#include "HashEngine.h"
#include <QHash>

bool HashEngine::check(QString file){
	QList<QString> HashesToScann;
	HashesToScann.append(gethash(file, QCryptographicHash::Algorithm::Md5).toHex());
	HashesToScann.append(gethash(file, QCryptographicHash::Algorithm::Sha1).toHex());
	HashesToScann.append(gethash(file, QCryptographicHash::Algorithm::Sha256).toHex());
	//qDebug() << HashesToScann;	
	return db->lookupByHashes(HashesToScann);
}

HashEngine::HashEngine(){
	db = new HashDB("../resources/hashes.db");
}
//make hash from file
QByteArray HashEngine::gethash(QString filepath, QCryptographicHash::Algorithm algorithm)
{

	QFile f(filepath);
	if (!f.open(QFile::ReadOnly))
	{
		throw std::invalid_argument("File not accessable");
	}
	
	QCryptographicHash hash(algorithm);
	if (hash.addData(&f)) {
		return hash.result();
	}
}