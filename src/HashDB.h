#pragma once
#include <QtSql>
#include <qlist.h>
class HashDB
{
	QSqlDatabase _db;
public:
	HashDB(QString dbFile);
	HashDB();
	~HashDB();
	bool lookupByHashes(QList<QString> hashes);
};
