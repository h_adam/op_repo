#include "HashDB.h"

#define HASHDBDEFAULTFILE "hashes.db"

HashDB::HashDB(QString dbFile)
{
	_db = QSqlDatabase::addDatabase("QSQLITE");
	_db.setDatabaseName(dbFile);
	if(!_db.open()) throw std::invalid_argument("DB file not accessable");
}
HashDB::HashDB()
{
	_db = QSqlDatabase::addDatabase("QSQLITE");
	_db.setDatabaseName(HASHDBDEFAULTFILE);
	if(!_db.open()) throw std::invalid_argument("Default database not found");
}



HashDB::~HashDB()
{
	_db.close();
}

bool HashDB::lookupByHashes(QList<QString> hashes)
{
	QSqlQuery query;
	query.prepare("SELECT COUNT(id) as `0` FROM hashes WHERE md5 = (:md5) AND sha1 = (:sha1) AND sha256 = (:sha256)");
	query.bindValue(":md5", hashes[0]);
	query.bindValue(":sha1", hashes[1]);
	query.bindValue(":sha256", hashes[2]);
	query.exec();
	query.first();

	if (query.value(0).toString().toInt() >= 1)
		return true;
	else
		return false;

}