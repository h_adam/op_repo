#pragma once
#include <qdebug.h>
#include <qfile.h>
#include <QCryptographicHash>
#include "Engine.h"
#include "HashDB.h"

class HashEngine : public Engine{
public:
	HashEngine();
	bool check(QString file);
protected:
	HashDB *db;
	QByteArray gethash(QString filepath, QCryptographicHash::Algorithm algorithm);
};